package config

const (
	//Resources-----------------------------------------
	Category = "category"
	Product  = "product"
	Account  = "account"
	Profile  = "profile"

	//Actions-------------------------------------------
	Read   = "read"
	Insert = "insert"
	Delete = "delete"
	Upadte = "update"
)
