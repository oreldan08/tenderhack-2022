package utils

const (

	//TODO all env names at one place
	ListenAddressEnvKey = "PORT"

	StrategyManagementBaseUrlEnvKey = "STRATEGY_MANAGEMENT_BASE_URL"

	//LogLevelEnvKey = "LOG_LEVEL"

	SecurityTokenTTLKey = "TOKEN_TTL"

	AppVersionEnvKey  = "APP_VERSION"
	ServiceNameEnvKey = "SERVICE_NAME"

	SessionUpdateFrequencyEnvKey = "SESSION_UPDATE_FREQUENCY"
)
