CREATE TABLE IF NOT EXISTS categories (
    id INT GENERATED ALWAYS AS IDENTITY,
    category_name VARCHAR,
    description TEXT,
    PRIMARY KEY (id)
);