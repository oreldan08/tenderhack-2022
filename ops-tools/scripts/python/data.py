register_data = [
  {
    "login": "denis",
    "password": "password",
    "role": "provider"
  },
  {
    "login": "daniil",
    "password": "password",
    "role": "provider"
  },
  {
    "login": "vitaliy",
    "password": "password",
    "role": "provider"
  }
]

profile_data = [
  {
    "organizationName": "denis",
    "organizationType": "denis"
  },
  {
    "organizationName": "daniil",
    "organizationType": "daniil"
  },
  {
    "organizationName": "vitaliy",
    "organizationType": "vitaliy"
  }
]