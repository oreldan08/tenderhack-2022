import requests
import json
import time
from data import register_data, profile_data

URL = "https://bbar2tj7t4pjvsbeu814.containers.yandexcloud.net"
HEADERS = {'Content-type': 'application/json', 'Accept': 'text/plain'}

REGISTER = "register"
PROFILE = "api/v1/profile"
PRODUCTS = "api/v1/products"
SESSIONS = "api/v1/sessions"
BETS = "api/v1/bets"

# for reg in register_data:
#   response = requests.post(f"{URL}/{REGISTER}", headers=HEADERS, json=reg)

start = time.time()
response = requests.get(f"{URL}/{SESSIONS}/1")
finish = time.time()

print(response.text)
print(finish - start)

start = time.time()
response = requests.get(f"{URL}/{BETS}/1")
finish = time.time()

print(response.text)
print(finish - start)

start = time.time()
response = requests.get(f"{URL}/{BETS}/2")
finish = time.time()

print(response.text)
print(finish - start)

start = time.time()
response = requests.get(f"{URL}/{BETS}/3")
finish = time.time()

print(response.text)
print(finish - start)



