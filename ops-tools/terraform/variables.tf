variable "token" {
  type      = string
  sensitive = true
}

variable "cloud_id" {
  type      = string
  sensitive = true
}

variable "folder_id" {
  type      = string
  sensitive = true
}

variable "zone" {
  type    = string
  default = "ru-central1-b"
}

variable "subnet" {
  type    = string
  default = "e2ljabm9s842nmi0ffbr"
}

variable "postgres_host" {
  type = string
  sensitive = true
}

variable "postgres_port" {
  type = string
  sensitive = true
}

variable "postgres_user" {
  type = string
  sensitive = true
}

variable "postgres_pass" {
  type = string
  sensitive = true
}

variable "postgres_db" {
  type = string
  sensitive = true
}