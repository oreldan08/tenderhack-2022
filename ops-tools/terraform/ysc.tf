resource "yandex_serverless_container" "go-service" {
  name               = "go-service"
  description        = "Tenderhack Go Service"
  memory             = 1024
  execution_timeout  = "10s"
  cores              = 1
  core_fraction      = 100
  service_account_id = "ajeerpjtnbc4lugjvrqo"
  image {
    url = "cr.yandex/crpk1mco8dc80hntnr5c/go-db-service:latest"
    environment = {
      GIN_MODE = "debug"
      LOG_LEVEL = "DEBUG" 
      POSTGRES_HOST = var.postgres_host 
      POSTGRES_PORT = var.postgres_port
      POSTGRES_DB = var.postgres_db 
      POSTGRES_USERNAME = var.postgres_user 
      POSTGRES_PASSWORD = var.postgres_pass 
      POSTGRES_SSL_MODE = "disable"
      POSTGRES_CONNECTION_TIMEOUT = "30"
      APP_VERSION = "1.0.0"
      GOCACHE = "/root/go/cache"
    }
  }
}